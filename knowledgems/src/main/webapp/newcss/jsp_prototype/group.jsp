<%--
  Created by IntelliJ IDEA.
  User: 1
  Date: 2021/7/19
  Time: 11:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
  <title>Title</title>
  <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="../css/bootstrap-grid.css">
  <link rel="stylesheet" type="text/css" href="../css/bootstrap-reboot.css">
  <link rel="stylesheet" type="text/css" href="../css/mycss.css">
</head>
<body>
<div class="dx-main">
  <!-- START: Navbar -->
  <c:import url="_navbar.jsp?menu=group"/>
  <!-- END: Navbar -->
  <div class="dx-box-6 bg-grey-6 mt-85">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
              <div class="row">
                  <div class="col-12 col-md-6 col-lg-4">
                    <div class="dx-portfolio-item">
                      <div class="dx-blog-item dx-box">
                        <div class="dx-blog-item-cont">
                          <h2><a class="mb-30 text-dark mnt-7">测试组名</a></h2>
                          <ul class="dx-blog-item-info">
                            <li>群主：测试</li>
                            <li>成员数: 5</li>
                            <li>创建时间: 2020-09-23</li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
        </div>
      </div>
    </div>
  </div>
  <!-- START: Footer -->
  <c:import url="_footer.jsp"/>
  <!-- END: Footer -->
</div>
</body>
</html>
