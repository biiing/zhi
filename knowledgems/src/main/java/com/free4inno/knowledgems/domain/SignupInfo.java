package com.free4inno.knowledgems.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import java.sql.Timestamp;

/**
 * Author HUYUZHU.
 * Date 2021/1/29 16:12.
 */
@Entity
@Table(name = "signup_info")
@Data
public class SignupInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id; //主键

    @Column(name = "name")
    private String name; //真实姓名

    @Column(name = "telnumber")
    private String telnumber; //手机号码

    @Column(name = "mail")
    private String mail; //邮箱

    @Column(name = "reason", columnDefinition = "TEXT")
    private String reason; //申请理由

    @Column(name = "status")
    private Integer status; //申请状态（0为待审核，1为已通过，2为已拒绝，3为其他）

    @Column(name = "signup_time")
    private Timestamp signupTime; //申请时间

    @Column(name = "process_time")
    private Timestamp processTime; //处理时间

    @Column(name = "crop_id")
    private Integer cropId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelnumber() {
        return telnumber;
    }

    public void setTelnumber(String telnumber) {
        this.telnumber = telnumber;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Timestamp getSignupTime() {
        return signupTime;
    }

    public void setSignupTime(Timestamp signupTime) {
        this.signupTime = signupTime;
    }

    public Timestamp getProcessTime() {
        return processTime;
    }

    public void setProcessTime(Timestamp processTime) {
        this.processTime = processTime;
    }

    public Integer getCropId() {
        return cropId;
    }

    public void setCropId(Integer cropId) {
        this.cropId = cropId;
    }
}
