package com.free4inno.knowledgems.domain;

import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import javax.persistence.Id;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Map;

/**
 * ResourceES.
 */

@Document(indexName = "knowledge_ms_v3", type = "resource")
public class ResourceES {
    private Integer id;
    private Integer crop_id;
    private Integer user_id;
    private Timestamp create_time;
    private Integer resourceId;
    private Timestamp edit_time;
    @Field(type = FieldType.Auto, searchAnalyzer = "ik_smart", analyzer = "ik_max_word")
    private String title;
    @Field(type = FieldType.Auto, searchAnalyzer = "ik_smart", analyzer = "ik_max_word")
    private String text;
    private String attach_text;
    private ArrayList<Object> attachment;
    private Map<String, HighlightField> highlight;
    private Map<String, SearchHits> innerHitsContent;
    private Integer attach_search_flag;
    private Integer comment_search_flag;
    private Integer text_search_flag;
    private ArrayList<Object> comment;
    private Integer superior;
    private Integer recognition;
    private Integer opposition;
    private Integer pageview;
    private Integer collection;
    @Field(type = FieldType.Auto, analyzer = "douhao")
    private String group_id;
    @Field(type = FieldType.Auto, analyzer = "douhao")
    private String label_id;
    private String permissionId;
    @Transient
    private String user_name;
    @Transient
    private String group_name;
    @Transient
    private String label_name;


    @Id
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Map<String, SearchHits> getInnerHitsContent() {
        return innerHitsContent;
    }

    public Integer getAttach_search_flag() {
        return attach_search_flag;
    }

    public void setAttach_search_flag(Integer attach_search_flag) {
        this.attach_search_flag = attach_search_flag;
    }

    public Integer getComment_search_flag() {
        return comment_search_flag;
    }

    public void setComment_search_flag(Integer comment_search_flag) {
        this.comment_search_flag = comment_search_flag;
    }

    public Integer getText_search_flag() {
        return text_search_flag;
    }

    public void setText_search_flag(Integer text_search_flag) {
        this.text_search_flag = text_search_flag;
    }

    public void setInnerHitsContent(Map<String, SearchHits> innerHitsContent) {
        this.innerHitsContent = innerHitsContent;
    }

    public Map<String, HighlightField> getHighlight() {
        return highlight;
    }

    public void setHighlight(Map<String, HighlightField> highlight) {
        this.highlight = highlight;
    }

    public ArrayList<Object> getComment() {
        return comment;
    }

    public void setComment(ArrayList<Object> comment) {
        this.comment = comment;
    }

    public Integer getResourceId() {
        return resourceId;
    }

    public void setResourceId(Integer resourceId) {
        this.resourceId = resourceId;
    }

    public String getAttach_text() {
        return attach_text;
    }

    public void setAttach_text(String attach_text) {
        this.attach_text = attach_text;
    }

    public String getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(String permissionId) {
        this.permissionId = permissionId;
    }

    public Integer getCrop_id() {
        return crop_id;
    }

    public void setCrop_id(Integer crop_id) {
        this.crop_id = crop_id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Timestamp getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Timestamp create_time) {
        this.create_time = create_time;
    }

    public Timestamp getEdit_time() {
        return edit_time;
    }

    public void setEdit_time(Timestamp edit_time) {
        this.edit_time = edit_time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ArrayList<Object> getAttachment() {
        return attachment;
    }

    public void setAttachment(ArrayList<Object> attachment) {
        this.attachment = attachment;
    }

    public Integer getSuperior() {
        return superior;
    }

    public void setSuperior(Integer superior) {
        this.superior = superior;
    }

    public Integer getRecognition() {
        return recognition;
    }

    public void setRecognition(Integer recognition) {
        this.recognition = recognition;
    }

    public Integer getOpposition() {
        return opposition;
    }

    public void setOpposition(Integer opposition) {
        this.opposition = opposition;
    }

    public Integer getPageview() {
        return pageview;
    }

    public void setPageview(Integer pageview) {
        this.pageview = pageview;
    }

    public Integer getCollection() {
        return collection;
    }

    public void setCollection(Integer collection) {
        this.collection = collection;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getLabel_id() {
        return label_id;
    }

    public void setLabel_id(String label_id) {
        this.label_id = label_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public String getLabel_name() {
        return label_name;
    }

    public void setLabel_name(String label_name) {
        this.label_name = label_name;
    }
}
