package com.free4inno.knowledgems.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Author HaoYi.
 * Date 2020/9/11.
 */
@Entity
@Table(name = "user")
@Data
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Integer id; //主键，用户Id

    @Column(name = "crop_id")
    private Integer cropId; //预留字段

    @Column(name = "account")
    private String account; //账户（手机号）

    @Column(name = "user_password")
    private String userPassword; //用户密码

    @Column(name = "real_name")
    private String realName; //真实姓名

    @Column(name = "mail")
    private String mail; //邮箱

    @Column(name = "account_name")
    private String accountName; //账户名（昵称）

    @Column(name = "role_id")
    private Integer roleId; //用户类型：0普通，1超级管理员，2用户管理员，3内容管理员，4系统管理员

    @Column(name = "app_key")
    private String appKey;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCropId() {
        return cropId;
    }

    public void setCropId(Integer cropId) {
        this.cropId = cropId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public User() {

    }

    public User(int id) {
        this.id = id;
    }
}
