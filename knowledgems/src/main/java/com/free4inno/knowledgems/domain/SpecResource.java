package com.free4inno.knowledgems.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import java.sql.Timestamp;

/**
 * Author: HaoYi.
 * Date: 2021/02/07.
 */
@Entity
@Table(name = "spec_resource")
@Data
public class SpecResource {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;  //主键

    @Column(name = "resource_id")
    private Integer resourceId;  //资源ID

    @Column(name = "spec_order")
    private Integer order;  //资源排序

    @Column(name = "type")
    private Integer type;  //1表示推荐，2表示公告

    @Column(name = "insert_time")
    private Timestamp insert_time;  //1表示推荐，2表示公告

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getResourceId() {
        return resourceId;
    }

    public void setResourceId(Integer resourceId) {
        this.resourceId = resourceId;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Timestamp getInsert_time() {
        return insert_time;
    }

    public void setInsert_time(Timestamp insert_time) {
        this.insert_time = insert_time;
    }

    public SpecResource() {

    }

    public SpecResource(int id) {
        this.id = id;
    }
}
