package com.free4inno.kmstika.dao;

import com.free4inno.kmstika.domain.Attachment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Author HUYUZHU.
 * Date 2021/3/26 11:33.
 */

@Repository
public interface AttachmentDao extends JpaRepository<Attachment, Integer> {
    List<Attachment> findAllByStatusIn(List<Attachment.AttachmentEnum> statusList);
}
